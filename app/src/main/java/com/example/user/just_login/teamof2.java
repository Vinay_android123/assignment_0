package com.example.user.just_login;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public  class teamof2 extends AppCompatActivity  implements View.OnClickListener{
    public static final String  url="http://agni.iitd.ernet.in/cop290/assign0/register/";
    Button submit;
    EditText  entry01, name01, entry02, name02;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       setContentView(R.layout.activity_teamof2);
//Extracting data from layout-------------------------------------
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        submit = (Button) findViewById(R.id.submit);
        entry01 = (EditText) findViewById(R.id.entry01);
        entry02 = (EditText) findViewById(R.id.entry02);
        name01 = (EditText) findViewById(R.id.name01);
        name02 = (EditText) findViewById(R.id.name01);

        submit.setOnClickListener((View.OnClickListener) this);
        };
    private void submitdata() {
        Intent intent=getIntent();
        final String teamname0=intent.getExtras().getString("myteamname").trim();
       final String teamname_key = teamname0;
        final String entry1_key = entry01.getText().toString().trim();
        final String entry2_key = entry02.getText().toString().trim();
        final String name1_key = name01.getText().toString().trim();
        final String name2_key = name02.getText().toString().trim();
        //generating a string requiest----------------------------------------------------
        StringRequest stringRequest= new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {



                        Log.e("log",response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(teamof2.this,error.toString(),Toast.LENGTH_LONG).show();
                    }
                })

        {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("teamname",teamname_key);
                params.put("entry1",entry1_key);
                params.put( "name1",name1_key);
                params.put("entry2",entry2_key);
                params.put("name2",name2_key);
                params.put( "entry3","");
                params.put( "name3","");
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submit:
                submitdata();
                break;
        }
      }
    }


