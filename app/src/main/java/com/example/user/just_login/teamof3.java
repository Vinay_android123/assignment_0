package com.example.user.just_login;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
//import android.R;


public  class teamof3 extends AppCompatActivity  implements View.OnClickListener{
    public static final String  url="http://agni.iitd.ernet.in/cop290/assign0/register/";
    Button submit;
    EditText  entry01, name01, entry02, name02,entry03, name03;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teamof3);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        submit = (Button) findViewById(R.id.submit);
        entry01 = (EditText) findViewById(R.id.entry01);
        entry02 = (EditText) findViewById(R.id.entry02);
        name01 = (EditText) findViewById(R.id.name01);
        name02 = (EditText) findViewById(R.id.name01);
        entry03 = (EditText) findViewById(R.id.entry03);
        name03 = (EditText) findViewById(R.id.name03);
        submit.setOnClickListener((View.OnClickListener) this);
    };
    private void submitdata() {
        Intent intent = getIntent();
        final String teamname0 = intent.getExtras().getString("myteamname").trim();
        final String teamname_key = teamname0;
        final String entry1_key = entry01.getText().toString().trim();
        final String entry2_key = entry02.getText().toString().trim();
        final String name3_key = name03.getText().toString().trim();
        final String name1_key = name01.getText().toString().trim();
        final String name2_key = name02.getText().toString().trim();
        final String entry3_key = entry03.getText().toString().trim();

        //-----------------------------validation data-----------------------------------
        String[] values = {entry1_key, entry2_key, entry3_key, name1_key, name2_key, name3_key};
        int i = 0;
        boolean[] valid = {false,false,false};
        while (i < 3) {
            if(values[i].length()!=11){
            char[] now = values[i].toCharArray();
            if ((((int) now[0]) == 50) && (((int) now[1]) == 48) && (((int) now[2]) >= 48) && (((int) now[2]) <= 57) && (((int) now[3]) >= 48) && (((int) now[3]) <= 57)) {
                if (((((int) now[4]) >= 65) && (((int) now[4]) <= 90) || ((((int) now[4]) >= 97) && (((int) now[4]) <= 122))) && ((((int) now[5]) >= 65) && (((int) now[5]) <= 90) || ((((int) now[5]) >= 97) && (((int) now[5]) <= 122)))) {
                    int j = 6;
                    while (j < 11) {
                        if ((((int) now[j]) >= 48) && (((int) now[j]) <= 57)) {
                            valid[i] = true;
                        }
                    }
                }

            }
            if(!valid[i]){
                break;
            }
            else {
                i++;
            }
        }
            int k = 3;
            while (k <6) {
                char[] now = values[k].toCharArray();
                for (int j = 0; j < values[k].length(); j++) {
                    if (((((int) now[j]) >= 65) && (((int) now[j]) <= 90) || ((((int) now[j]) >= 97) && (((int) now[j]) <= 122)))) {
                        valid[k] = true;
                    } else {
                        valid[k] = false;
                    }
                }
                if (!valid[k]) {
                    break;
                }
            }

                    if ((((int) now[0]) == 50) && (((int) now[1]) == 48) && (((int) now[2]) >= 48) && (((int) now[2]) <= 57) && (((int) now[3]) >= 48) && (((int) now[3]) <= 57)) {
                        if (((((int) now[4]) >= 65) && (((int) now[4]) <= 90) || ((((int) now[4]) >= 97) && (((int) now[4]) <= 122))) && ) {
                            int j = 6;
                            while (j < 11) {
                                if ((((int) now[j]) >= 48) && (((int) now[j]) <= 57)) {
                                    valid[i] = true;
                                }
                            }
                        }

                    }

                    else {
                        i++;
                    }
                }
        if (i<3) {
            Toast.makeText(teamof3.this, "Error:Entry Number"+(i+1)+"is not valid!!", Toast.LENGTH_LONG).show();
        } else {









        }
            //generating a string requiest----------------------------------------------------

            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @Override
                      public void onResponse(String response) {
                            try{
                                JSONObject j_respose=new JSONObject(response);
                                String message=j_respose.getString("RESPONSE_MESSAGE");
                                Toast.makeText(teamof3.this, message, Toast.LENGTH_LONG).show();
                            }catch(JSONException e){e.printStackTrace();}

                            Log.e("log", response);
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(teamof3.this, error.toString(), Toast.LENGTH_LONG).show();
                        }
                    })

            {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("teamname", teamname_key);
                    params.put("entry1", entry1_key);
                    params.put("name1", name1_key);
                    params.put("entry2", entry2_key);
                    params.put("name2", name2_key);
                    params.put("entry3", entry3_key);
                    params.put("name3", name3_key);
                    return params;
                }
            };
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }
    }
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.submit:
                submitdata();
                break;
        }
    }



}
